import GObject from 'gi://GObject';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';

import * as Confirm_Dialog from './dialogs/confirm.js';

import * as Docker from '../lib/docker.js';

export const Image_Menu = GObject.registerClass(
	class Image_Menu extends PopupMenu.PopupSubMenuMenuItem {
		_init(image) {
			super._init(image.name);

			// Set size of sub menu. !important
			this.menu.actor.style = `min-height: ${image.settings.get_int('submenu-text')}px;`;

			this._remove = new PopupMenu.PopupMenuItem(_("Run"));
			this._remove.connect('activate', () => Docker.run_command(Docker.docker_commands.i_run, image));
			this.menu.addMenuItem(this._remove);

			this._remove = new PopupMenu.PopupMenuItem(_("Run interactive"));
			this._remove.connect('activate', () => Docker.run_command(Docker.docker_commands.i_run_i, image));
			this.menu.addMenuItem(this._remove);

			this._inspect = new PopupMenu.PopupMenuItem(_("Inspect"));
			this._inspect.connect('activate', () => Docker.run_command(Docker.docker_commands.i_inspect, image));
			this.menu.addMenuItem(this._inspect);

			this._remove = new PopupMenu.PopupMenuItem(Docker.docker_commands.i_rm.label);
			this._remove.connect(
				'activate',
				() => Confirm_Dialog.open(
					Docker.docker_commands.i_rm.label, // Dialog title
					`Are you sure you want to ${Docker.docker_commands.i_rm.label}?`, // Description
					() => Docker.run_command(Docker.docker_commands.i_rm, image),
				)
			);
			this.menu.addMenuItem(this._remove);
		}
	}
)