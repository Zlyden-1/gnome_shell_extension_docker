import GObject from 'gi://GObject';

import * as Confirm_Dialog from './dialogs/confirm.js';
import * as Me_PopupMenu from './popupMenu.js';

import * as Docker from '../lib/docker.js';

export const Image_Menu = GObject.registerClass(
	class Image_Menu extends Me_PopupMenu.PopupSubMenuMenuItem {
		_init(image) {
			super._init(image.name);

			// Set size of sub menu. !important
			this.menu.actor.style = `min-height: ${image.settings.get_int('submenu-image')}px;`;

			this.new_action_button("media-playback-start", () => {
				Docker.run_command(Docker.docker_commands.i_run, image);
			}, _("Run"));

			this.new_action_button("utilities-terminal", () => {
				Docker.run_command(Docker.docker_commands.i_run_i, image);
			}, _("Run interactive"));

			this.new_action_button("user-info", () => {
				Docker.run_command(Docker.docker_commands.i_inspect, image);
			}, _("Inspect"));

			this.new_action_button(
				"edit-delete",
				() => Confirm_Dialog.open(
					Docker.docker_commands.i_rm.label, // Dialog title
					`Are you sure you want to ${Docker.docker_commands.i_rm.label}?`, // Description
					() => Docker.run_command(Docker.docker_commands.i_rm, image),
				),
				Docker.docker_commands.i_rm.label
			);
		}
	}
)