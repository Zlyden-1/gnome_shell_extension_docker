import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';
import Menu from './modules/menu.js';
import DockerManager from './lib/dockerManager.js';


export default class DockerExtension extends Extension{
	constructor(metadata) {
        super(metadata);
    }

	enable() {
        this._dokcerManager = new DockerManager(this);
		this._settings = this.getSettings();
		this._indicator = new Menu();

		Main.panel.addToStatusArea(this.uuid, this._indicator);
	}

	disable() {
		this._indicator.destroy();
		this._indicator = null;
		
        this._settings = null;

        this._dokcerManager.destroy();
        this._dokcerManager = null;
	}
}