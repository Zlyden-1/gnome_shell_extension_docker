# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-09-11 19:03+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/modules/containerMenuIcons.js:25 src/modules/containerMenuText.js:26
msgid "Exec Bash"
msgstr ""

#: src/modules/containerMenuIcons.js:29 src/modules/containerMenuText.js:30
msgid "Attach Terminal"
msgstr ""

#: src/modules/containerMenuIcons.js:33 src/modules/containerMenuText.js:34
msgid "Pause"
msgstr ""

#: src/modules/containerMenuIcons.js:37 src/modules/containerMenuIcons.js:52
#: src/modules/containerMenuText.js:38 src/modules/containerMenuText.js:52
msgid "Stop"
msgstr ""

#: src/modules/containerMenuIcons.js:41 src/modules/containerMenuIcons.js:56
#: src/modules/containerMenuText.js:42 src/modules/containerMenuText.js:56
msgid "Restart"
msgstr ""

#: src/modules/containerMenuIcons.js:48 src/modules/containerMenuText.js:48
msgid "Unpause"
msgstr ""

#: src/modules/containerMenuIcons.js:62 src/modules/containerMenuText.js:62
msgid "Start"
msgstr ""

#: src/modules/containerMenuIcons.js:66 src/modules/containerMenuText.js:66
msgid "Start interactive"
msgstr ""

#: src/modules/containerMenuIcons.js:71 src/modules/containerMenuText.js:71
msgid "View Logs"
msgstr ""

#: src/modules/containerMenuIcons.js:75 src/modules/containerMenuText.js:75
#: src/modules/imageMenuIcons.js:26 src/modules/imageMenuText.js:24
msgid "Inspect"
msgstr ""

#: src/modules/containerMenu.js:51
msgid "Information"
msgstr ""

#: src/modules/containerMenu.js:58
msgid "IP copied to clipboard"
msgstr ""

#: src/modules/containerMenu.js:66
msgid "Gateway copied to clipboard"
msgstr ""

#: src/modules/containerMenu.js:82
msgid "Ports"
msgstr ""

#: src/modules/imageMenuIcons.js:18 src/modules/imageMenuText.js:16
msgid "Run"
msgstr ""

#: src/modules/imageMenuIcons.js:22 src/modules/imageMenuText.js:20
msgid "Run interactive"
msgstr ""

#: src/modules/menu.js:20
msgid "Docker Menu"
msgstr ""
